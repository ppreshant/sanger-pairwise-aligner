require 'csv'
require 'bio'
require 'fileutils'


# parsing alignment files?
def read_out(file, ref_id, query_id)
  results = {'identity' => '', 'similarity' => '', 'gaps' => ''}
  results['ref'] = {'start' => nil, 'stop' => nil, 'seq' => ''}
  results['query'] = {'start' => nil, 'stop' => nil, 'seq' => ''}

  File.open(file, 'r').each_line do |l|
    if l[0] == '#'
      if l.match(/Identity/)
        results['identity'] = l.split(' ')[-2,2].join('')
      elsif l.match(/Similarity/)
        results['similarity'] = l.split(' ')[-2,2].join('')
      elsif l.match(/Gaps/)
        results['gaps'] = l.split(' ')[-2,2].join('')
      end
    else 
      items = l.split(' ')
      if items.size == 4 and !l.match(/\|/)
        if items[0].downcase == ref_id[0, 13].downcase
          results['ref']['start'] = items[1].to_i if results['ref']['start'].nil?
          results['ref']['stop'] = items[3].to_i
          results['ref']['seq'] += items[2]
        elsif items[0].downcase == query_id[0, 13].downcase
          results['query']['start'] = items[1].to_i if results['query']['start'].nil?
          results['query']['stop'] = items[3].to_i
          results['query']['seq'] += items[2]
        end
      end
    end
  end
  return results
end

# Reads an alignment and returns and quality aware summary of deletion, substitution and insertion?
def get_muts(align, qual_mat)
  ref = align['ref']
  query = align['query']
  muts = {'del' => [], 'sub' => [], 'ins' => []}

  current_ref_inx = ref['start']
  current_query_inx = query['start']
  this_m = {'type' => nil, 'inx' => nil, 'from' => '', 'to' => '', 'qual' => []}
  
  (0..(ref['seq'].size-1)).to_a.each do |i| # loop over each alignment letter
    if ref['seq'][i] != query['seq'][i]
      if ref['seq'][i] == '-'
        if this_m['type'] != 'ins'
          muts[this_m['type']] << {'inx' => this_m['inx'], 'from' => this_m['from'], 'to' => this_m['to'], 'qual' => this_m['qual'].instance_eval { reduce(:+) / size.to_f }} unless this_m['type'].nil?
          this_m = {'type' => 'ins', 'inx' => current_ref_inx, 'from' => '', 'to' => '', 'qual' => []}
        end
        this_m['to'] += query['seq'][i]
        this_m['qual'] << qual_mat[current_query_inx]
        current_query_inx += 1

      elsif query['seq'][i] == '-'
        if this_m['type'] != 'del'
          muts[this_m['type']] << {'inx' => this_m['inx'], 'from' => this_m['from'], 'to' => this_m['to'], 'qual' => this_m['qual'].instance_eval { reduce(:+) / size.to_f }} unless this_m['type'].nil?
          this_m = {'type' => 'del', 'inx' => [], 'from' => '', 'to' => '', 'qual' => []}
        end
        this_m['from'] += ref['seq'][i]
        this_m['inx'] << current_ref_inx
        this_m['qual'] << qual_mat[current_query_inx]
        current_ref_inx += 1

      else
        muts[this_m['type']] << {'inx' => this_m['inx'], 'from' => this_m['from'], 'to' => this_m['to'], 'qual' => this_m['qual'].instance_eval { reduce(:+) / size.to_f }} unless this_m['type'].nil?
        this_m = {'type' => 'sub', 'inx' => current_ref_inx, 'from' => ref['seq'][i], 'to' => query['seq'][i], 'qual' => []}
        this_m['qual'] << qual_mat[current_query_inx]
        current_ref_inx += 1
        current_query_inx += 1
      end

    else
      current_ref_inx += 1
      current_query_inx += 1
    end
  end
  muts[this_m['type']] << {'inx' => this_m['inx'], 'from' => this_m['from'], 'to' => this_m['to'], 'qual' => this_m['qual'].instance_eval { reduce(:+) / size.to_f }} unless this_m['type'].nil?
  return muts
end

# Read ABI files ; returning quality and a QC verdict?
def handle_abi(abi_f, type, fa_f, id)
  obj = Bio::Abif.open(abi_f)
  # create fasta file
  len = 0
  qual = []
  qual_qc = 'PASS'
  obj.each do |o|
    o.complement! if type == 'R'
    seq = o.to_seq[50, 900]

    unless seq
      unless File.exist?(fa_f)
        f = File.new(fa_f, 'w')
        f.puts ''
        f.close
      end
      return ['empty', []]
    end

    qual = o.qualities[50, 900]
    if qual.nil?
      qual_qc = 'FAIL'
      qual = []
    end

    cnt = 0
    qual.each {|q| cnt+=1 if q >= 20}
    if cnt < 500
      qual_qc = 'FAIL'
    end

    unless File.exist?(fa_f)
      f = File.new(fa_f, 'w')
      f.puts seq.to_fasta(id, 80)
      f.close
    end
  end

  return [qual_qc, qual]
end

################MAIN#####################
pl = ARGV[0]
query_path = 'query_seqs/' + pl
system("mkdir query_seqs") unless File.exist?('query_seqs')
FileUtils.mkpath(query_path)unless File.exist?(query_path)

align_path = 'align_outs/' + pl
system("mkdir align_outs") unless File.exist?('align_outs')
FileUtils.mkpath(align_path) unless File.exist?(align_path)

re_path = 'results'
FileUtils.mkpath(re_path) unless File.exist?(re_path)

out = CSV.open("#{re_path}/#{pl}.csv", 'w')
out << ['ab1', 'clone_id', 'identity', 'read quality', 'substitutions', 'deletions', 'insertions']

primers = {}
CSV.foreach('data/primer_orig.csv') do |row|
  primers[row[0].downcase] = row[1].size
end
primers['m13'] = 0

CSV.foreach("data/layouts/#{pl}.csv") do |row|
  next if row[0] == 'ab1'
  query_id = row[0].split('.')[0]
  puts "working on #{query_id}"
  query_fa = "#{query_path}/#{query_id}.fa"
  read_qc, qual_mat = handle_abi("raw/#{pl}/#{row[0]}", row[-1].upcase, query_fa, query_id)
  ref_id = row[2]
  primer_id = row[3].downcase  # col after ref. old: row[2].split('_')[1].downcase
  ref_fa = "data/ref_seqs/#{ref_id}.fa"
  align_out = "#{align_path}/#{query_id}.txt"

# Local sequence alignment 
  unless File.exist?(align_out)
    Bio::EMBOSS.run('water', '-asequence', query_fa, '-bsequence', ref_fa, '-outfile', align_out)
  end

  align_results = read_out(align_out, ref_id, query_id)
  mutations = get_muts({'ref' => align_results['ref'], 'query' => align_results['query']}, qual_mat)
  new_row = [row[0], row[1], align_results['identity'], read_qc]
  
  if mutations['sub'].empty?
    new_row << ''
  else
    m = []
    mutations['sub'].each do |x|
      inx = x['inx'] - primers[primer_id]
      pct = '%.2f' % ((1 - 10**(-(x['qual']/10.0)))*100)
      if pct == '100.00'
        m << "#{x['from']}#{inx}#{x['to']}" if inx > 0 
      else
        m << "#{x['from']}#{inx}#{x['to']} (#{pct}%)" if inx > 0
      end
    end
    new_row << m.join('; ')
  end

  if mutations['del'].empty?
    new_row << ''
  else
    m = []
    mutations['del'].each do |x|
      inx = x['inx'][0] - primers[primer_id]
      pct = '%.2f' % ((1 - 10**(-(x['qual']/10.0)))*100)
      if pct == '100.00'
        m << "#{x['inx'][0]-primers[primer_id]}-#{x['inx'][-1]-primers[primer_id]}: #{x['from']}" if inx > 0
      else
        m << "#{x['inx'][0]-primers[primer_id]}-#{x['inx'][-1]-primers[primer_id]}: #{x['from']} (#{pct}%)" if inx > 0
      end
    end
    new_row << m.join('; ')
  end
  
  if mutations['ins'].empty?
    new_row << ''
  else
    m = []
    mutations['ins'].each do |x|
      inx = x['inx'] - primers[primer_id]
      pct = '%.2f' % ((1 - 10**(-(x['qual']/10.0)))*100)
      if pct == '100.00'
        m << "#{inx}: #{x['to']}" if inx > 0
      else
        m << "#{inx}: #{x['to']} (#{pct}%)" if inx > 0
      end
    end
    new_row << m.join('; ')
  end
  
  out << new_row
end
out.close
