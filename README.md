Author: Kun Yang (Postdoc, Joel Bader lab, Johns Hopkins University, 2016)
[Homepage](https://www.hopkinsmedicine.org/profiles/details/kun-yang) ; [google scholar](https://scholar.google.com/citations?user=ZVKWcokAAAAJ&hl=en&oi=ao)
_Readme and instructions by Prashant K_
Purpose: Aligns sanger sequencing chromatographs with designated sequence files in high throughput

# Overview

- run using this command `ruby align.rb plate1` from the commandline (`shell` / `cmd`/`powershell`). replace `plate1` with your data directory.
- Summary of the results will appear in the results folder as a summary spreadsheet

| ab1                   | clone_id | identity       | read quality | substitutions | deletions | insertions |
|-----------------------|----------|----------------|--------------|---------------|-----------|------------|
| HZPK_F1a_M13F_017.ab1 | F        | 845/846(99.9%) | PASS         | c755g         |           |            |
| HZPK_F1b_M13F_024.ab1 | F        | 846/847(99.9%) | PASS         | c755g         |           |            |

- Intermediate results such as the alignment files appear within the `align_outs/` folder as `.txt` files with the EMBOSS::water result
	- basecalled sequences from the `.ab1` files are stored as `.fa` within the `query_seqs/` folder

# Instructions

To analyze your own sequencing data, you need to create files in the "data" folder and put the raw ab1 sequencing data in the "raw" folder.  The analysis result will be created as a csv file in the "results" folder
> Source: Kun

1. `raw/` folder: Create a new directory with the `.ab1` sanger seq files
2. `data/primer_orig.csv`: Add your sequencing primer name and sequence to this list
3. `data/ref_seqs/`: Add the fasta files (`.fa` extension)
4. `data/layouts`:  copy the `plate1.csv` spreadsheet into a new file (_with the same name as the directory containing .ab1 files_). 
	- col 1: Fill in the name of the `.ab1` file
		- Use these bash commands from within the `raw/..` dir to get a list of `.ab1` files into your clipboard (_works on gitbash in windows 10_) 
		```sh
		ls | sed 's/ /\n/g' | clip
		```
	- col 2: _no idea what clone ID does, except just carry into the results as it is_
	- col 3: Reference sequence to do pairwise alignment with 
	- col 4: Name of the primer
	- col 5: Direction of the sequencing (F for forward and R for reverse). 

5. run the scrupt now using `ruby align.rb plate1` within the base folder
6. results will appear as a `.csv` file within the `results/` folder
- (_for testing_) Run the test file `plate1` to check for the code running. The expected result is already in the `results/` folder

- Before running the script for the first time, you need to install ruby and dependencies on your computer.

# Dependencies
1. Use conda to install ruby (or download through their website)
2. rubygems -- helps install other packages - https://rubygems.org/pages/download
3. Bioruby - http://bioruby.org/ . Install through gem install bio
4. CSV - gem install csv
5. Need to download EMBOSS suite locally (from : https://emboss.sourceforge.net/download/#Stable/) says: http://bioruby.org/rdoc/Bio/EMBOSS.html
	5b. Can use windows explorer to ftp and download the installer (.exe) file from (ftp://emboss.open-bio.org/pub/EMBOSS/windows/)

# Todo
- [ ] How to show substitutions that are more important than the ones on the edges of the alignment. _Ex. 80_Ec.. shows C788c (99.37%), ... 983 ; I presume these are on the edges?_
